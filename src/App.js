import React from "react";
import {BrowserRouter as Router,Route, Switch} from 'react-router-dom' ;
import inicio from "./Pages/inicio";
import 'antd/dist/antd.css';
import formulario from "./Pages/formulario";
import registro from "./Pages/registro";

function App() {
  return (
     <Router>
       <Route path="/" exact component={ inicio } />
       <Route path="/formulario"  component={ formulario } />
       <Route path="/registro"  component={ registro } />
     </Router>
  );
}

export default App;
