import {
  Button,
  Col,
  Row,
  Modal,
  Form,
  Input,
  Select,
  Switch,
  InputNumber,
  message,
  Spin,
} from "antd";
import React, { useEffect, useState } from "react";
import Containers from "../../components/Container/Containers";
import TableComponent from "../../components/table/TableComponent";
import "../../styles.css";
import { useDispatch, useSelector } from "react-redux";
import { INITIAL_REQUEST_STUDENTS_SUCCESS } from "../../redux/constants";
import { registerStudentSuccess } from "../../redux/actions";
import { formatCountdown } from "antd/lib/statistic/utils";
import { useForm } from "antd/lib/form/Form";
const { Option } = Select;

const Formulario = () => {
  const [form] = useForm();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: INITIAL_REQUEST_STUDENTS_SUCCESS });
  }, []);

  const { spin } = useSelector((data) => data);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const onFinish = (e) => {
    console.log(e);
    return new Promise((resolve, reject) => {
      dispatch(registerStudentSuccess({ ...e, resolve, reject }));
    })
      .then((res) => {
        message.success(res);
        handleOk();
      })
      .catch((err) => {
        message.error(err);
      });
  };
  return (
    <Row justify="center" style={{ paddingTop: "5rem" }}>
      <Col xl={22} lg={22} md={22} sm={22} xs={22}>
        <Containers>
          <Row justify="space-between">
            <Col xl={10} lg={10} md={10} sm={14} xs={22}>
              <h1 className="title-primary">TABLA DE ESTUDIANTES</h1>
            </Col>
            <Col xl={4} lg={4} md={4} sm={10} xs={22}>
              <Button type="primary" onClick={showModal}>
                Agregar estudiante
              </Button>
            </Col>
          </Row>

          <Modal
            visible={isModalVisible}
            title="Agregar estudiante"
            onOk={handleOk}
            onCancel={handleCancel}
            confirmLoading={spin}
            okText="Registrar"
            cancelText="cancelar"
            onOk={() => form.submit()}
          >
            <Spin spinning={spin} tip="Consultando la base de datos">
              <Form onFinish={onFinish} form={form}>
                <Form.Item
                  label="Nombre"
                  name="name"
                  rules={[
                    {
                      type: "string",
                      required: true,
                      message: "Porfavor ingrese su nombre",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Apellido"
                  name="lastName"
                  rules={[
                    {
                      type: "string",
                      required: true,
                      message: "Porfavor ingresse su apellido",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item label="Sexo" name="sex"
                 rules={[{required: true, message: 'Porfavor ingresse su sexo' }]}>
                  <Select>
                    <Option value="masculino">masculino</Option>
                    <Option value="femenino">femenino</Option>
                    <Option value="Otros">Otros</Option>
                  </Select>
                </Form.Item>
                  <Form.Item
                    label="Eres de ingenieria en sistemas"
                    name="enginer_system"
                  >
                    <Switch />
                  </Form.Item>
                  <Form.Item
                    name="age"
                    label="Edad"
                    rules={[
                      {
                        required: true,
                        type: "number",
                        min: 0,
                        max: 99,
                        message:
                          "Porfavor ingresse su edad mayor de 0 y menor de 99",
                      },
                    ]}
                  >
                    <InputNumber />
                  </Form.Item>
                  <Form.Item label="Ciudad" name="city"
                   rules={[{required: true, message: 'Porfavor ingresse su ciudad' }]}>
                    <Select>
                      <Option value="La paz">La paz</Option>
                      <Option value="Cochabamba">Cochabamba</Option>
                      <Option value="Santa Cruz">Santa Cruz</Option>
                      <Option value="Tarija">Tarija</Option>
                      <Option value="Oruro">Oruro</Option>
                      <Option value="Beni">Beni</Option>
                      <Option value="Pando">Pando</Option>
                      <Option value="Chuquisaca">Chuquisaca</Option>
                      <Option value="Potosi">Potosi</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item name="id">
                  </Form.Item>
              </Form>
            </Spin>
          </Modal>

          <TableComponent />
        </Containers>
      </Col>
    </Row>
  );
};

export default Formulario;
