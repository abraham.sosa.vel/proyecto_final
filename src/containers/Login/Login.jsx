import { Form, Input, Button, Col, Row, Spin, message } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {  Link, Redirect, useHistory } from "react-router-dom";
import Containers from "../../components/Container/Containers";
import {
  loginUserStart,
  loginUserSuccess,
} from "../../redux/actions";
import "../../styles.css";
const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const {spin,auth} = useSelector((data) => data);


  const onFinish = (e) => {
    return new Promise((resolve,reject)=>{
      dispatch(loginUserSuccess({e,resolve,reject}));
    //dispatch({type: 'LOGIN_USER_SUCCESS', payload: {e}})
    //dispatch(loginUserStart(e))
    })
    .then((res) => {
      message.success(res);
      history.push("/formulario", { from: "HomePage" })
    })
    .catch((err) => {
      message.error(err);
    });
  };


  const onFinishFailed = (e) => {
    console.log(e);
  };
  return (
    <>
      <Row justify="center" style={{ paddingTop: "7rem", color: "white" }}>
        <Col xl={14} lg={18} md={20} sm={20} xs={20}>
        <Spin spinning={spin} tip={"Conectando"}>
          <Containers>
            <Row justify="space-around">
              <Col xl={11} lg={11} md={20} sm={20} xs={20}>
                <img src="login.svg"></img>
              </Col>
              <Col
                xl={11}
                lg={11}
                md={20}
                sm={20}
                xs={20}
                style={{ textAlign: "center" }}
              >
                <h1 className="title-primary">INICIO DE SESION</h1>
                <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
                  <Form.Item
                    label="Correo electronico"
                    name="email"
                    rules={[
                      {
                        type: "email",
                        required: true,
                        message:
                          "Porfavor ingrese un correo electronico valido!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Contraseña"
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Porfavor ingrese su contraseña",
                      },
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>

                  <Form.Item style={{ marginTop: "2rem" }}>
                    <Button type="primary" htmlType="submit" block>
                      Ingresar
                    </Button>
                    <Link to="/registro"><Button block disabled={"true"}>Registrarse</Button>
                    </Link>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </Containers>
          </Spin>
        </Col>
      </Row>
    </>
  );
};

export default Login;
