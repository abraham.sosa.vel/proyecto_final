import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { DatePicker } from 'antd';
import { Provider } from 'react-redux';
import reducer from './redux/reducer';

import createSagaMiddleware from 'redux-saga'
import { applyMiddleware, createStore } from 'redux';
import { sagaLogin } from './redux/sagas';

const sagaMiddleware = createSagaMiddleware()
const store=createStore(reducer,applyMiddleware(sagaMiddleware))
sagaMiddleware.run(sagaLogin)
ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
  document.getElementById('root')
);
reportWebVitals();
