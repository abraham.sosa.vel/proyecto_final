import { Col, Row } from "antd";
import React from "react";

const Containers = ({ children }) => {
  return (
    <>
      <Row>
        <Col xl={24} lg={24} md={24} sm={24} xs={24} className="containersDiv">
            {children}
        </Col>
      </Row>
      <style jsx="true">{`
        .containersDiv {
          background-color: white;
          padding:2rem;
          border-radius:10px;
          box-shadow:0 0 10px #bfbfbf;

        }
      `}</style>
    </>
  );
};

export default Containers;
