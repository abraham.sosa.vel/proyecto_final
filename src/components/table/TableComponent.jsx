import React, { useState } from "react";
import { Table, Space, Spin,Modal,Form,Select,Input,InputNumber,Switch, message } from 'antd';
import { useDispatch, useSelector } from "react-redux";
import { deleteStudentSuccess, updateStudentSuccess } from "../../redux/actions";
import { useForm } from "antd/lib/form/Form";
const { Option } = Select;



const TableComponent = () => {
  const [form] = useForm();
  const {spin,data}=useSelector((state)=>state)
  const dispatch=useDispatch();
  const deleteStudent=(e)=>{
    dispatch(deleteStudentSuccess(e))  
  }


const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const onFinish=(e)=>{
   console.log(e)
   return new Promise((resolve,reject)=>{
    dispatch(updateStudentSuccess({...e,resolve,reject}))
  })
  .then(res=>{
    message.success(res);
    handleOk();
  })
  .catch(err=>{
    message.error(err)
  })
  }
  const updateStudent=({name,lastName,age,city,sex,enginer_system,id})=>{
    console.log(id)
    form.setFieldsValue({name,lastName,age,city,sex,enginer_system,id})
    showModal()
  }
const columns = [
  {
    title: 'Nombre',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Apellido',
    dataIndex: 'lastName',
    key: 'lastName',
  },
  {
    title: 'Edad',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Sexo',
    dataIndex: 'sex',
    key: 'sex',
  },
  {
    title: 'Ciudad',
    dataIndex: 'city',
    key: 'city',
  },
  {
    title: 'Id',
    dataIndex: 'id',
    key: 'id',
  },

  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <Space size="middle">
        <a onClick={()=>updateStudent(record)}>Editar </a>
        <a onClick={()=>deleteStudent(record)}>Eliminar</a>
      </Space>
    ),
  },
];

 return(
   <>
   <Spin spinning={spin} tip="Eliminando de la base de datos">
  <Table columns={columns} dataSource={data} />
  </Spin>


  <Modal
            visible={isModalVisible}
            title="Agregar estudiante"
            onOk={handleOk}
            onCancel={handleCancel}
            confirmLoading={spin}
            okText="Registrar"
            cancelText="cancelar"
            onOk={()=>form.submit()}
          >
    <Spin spinning={spin} tip="Consultando la base de datos">
    <Form onFinish={onFinish} form={form}>
      <Form.Item
        label="Nombre"
        name="name"
        rules={[{type:"string", required: true, message: 'Porfavor ingrese su nombre' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Apellido"
        name="lastName"
        rules={[{type:"string", required: true, message: 'Porfavor ingresse su apellido' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item label="Sexo" name="sex">
          <Select>
            <Option value="masculino">masculino</Option>
            <Option value="femenino">femenino</Option>
            <Option value="Otros">Otros</Option>
          </Select>
        </Form.Item>
      <Form.Item >
      <Form.Item label="Eres de ingenieria en sistemas" name="enginer_system">
          <Switch />
        </Form.Item>

        <Form.Item
        name="age"
        label="Edad"
        rules={[
          {
            type: 'number',
            min: 0,
            max: 99,
          },
        ]}
      >
        <InputNumber />
      </Form.Item>
      <Form.Item label="Ciudad" name="city">
          <Select>
            <Option value="La paz">La paz</Option>
            <Option value="Cochabamba">Cochabamba</Option>
            <Option value="Santa Cruz">Santa Cruz</Option>
            <Option value="Tarija">Tarija</Option>
            <Option value="Oruro">Oruro</Option>
            <Option value="Beni">Beni</Option>
            <Option value="Pando">Pando</Option>
            <Option value="Chuquisaca">Chuquisaca</Option>
            <Option value="Potosi">Potosi</Option>
            
          </Select>
        </Form.Item>
      </Form.Item>
      <Form.Item name="id">
      </Form.Item>
      </Form>
    </Spin>
          </Modal>
  </>
 )
};
export default TableComponent


