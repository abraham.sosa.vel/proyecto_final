import {
  DELETE_STUDENT_START,
  DELETE_STUDENT_SUCCESS,
  INITIAL_REQUEST_STUDENTS_START,
  LOGIN_USER_START,
  LOGIN_USER_SUCCESS,
  REGISTER_STUDENT_START,
  REGISTER_STUDENT_SUCCESS,
  SPIN_FALSE,
  SPIN_TRUE,
  UPDATE_STUDENT_START,
  UPDATE_STUDENT_SUCCESS,
} from "./constants";

export const loginUserStart = (payload) => ({
  type: LOGIN_USER_START,
  payload,
});
export const loginUserSuccess = (payload) => ({
  type: LOGIN_USER_SUCCESS,
  payload,
});
export const spinTrue = () => ({
  type: SPIN_TRUE,
});
export const spinFalse = () => ({
  type: SPIN_FALSE,
});

export const initialRequestStart = (payload) => ({
  type: INITIAL_REQUEST_STUDENTS_START,
  payload,
});

export const registerStudentStart = (payload) => ({
  type: REGISTER_STUDENT_START,
  payload,
});
export const registerStudentSuccess = (payload) => ({
  type: REGISTER_STUDENT_SUCCESS,
  payload,
});
export const deleteStudentStart = (payload) => ({
  type: DELETE_STUDENT_START,
  payload,
});
export const deleteStudentSuccess = (payload) => ({
  type: DELETE_STUDENT_SUCCESS,
  payload,
});
export const updateStudentStart = (payload) => ({
  type: UPDATE_STUDENT_START,
  payload,
});
export const updateStudentSuccess = (payload) => ({
  type: UPDATE_STUDENT_SUCCESS,
  payload,
});
