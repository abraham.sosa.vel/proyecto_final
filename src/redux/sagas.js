import { message } from "antd";
import {
  put,
  takeLatest,
  call,
  all,
  select,
} from "redux-saga/effects";
import request, { postOptionsWithoutToken,getOptionsWithoutToken,deleteOptionsWithoutToken, patchOptionsWithoutToken } from "../utils/request";
import { initialRequestStart, loginUserStart, registerStudentStart, spinFalse, spinTrue,deleteStudentStart, updateStudentStart } from "./actions";
import { DELETE_STUDENT_SUCCESS, INITIAL_REQUEST_STUDENTS_SUCCESS, LOGIN_USER_START, LOGIN_USER_SUCCESS, REGISTER_STUDENT_SUCCESS, UPDATE_STUDENT_SUCCESS } from "./constants";

export function* loginUser({payload:{resolve,reject,e}}) {
    try {
        yield put(spinTrue());
        const url = "http://localhost:3000/api/Users/login";
        const option = postOptionsWithoutToken(e);
        const loginSuccess = yield call(request, url, option);
        yield all([put(loginUserStart({loginSuccess,e})),
                   put(spinFalse())
                ])
        yield call(resolve, "Bienvenido");
      } catch (e) {
        yield call(reject, "Usuario incorrecto");
        yield all([put(spinFalse())]);
      }
  
}

export function* initialRequestSaga(){
  try{
    yield put(spinTrue());
    const url ="http://localhost:3000/api/students";
    const option=getOptionsWithoutToken();
    const tableSuccess=yield call(request,url,option);
    yield all([
      put(initialRequestStart(tableSuccess)),
      put(spinFalse())
    ])
  }catch(e){
    put(spinFalse())
    message.error("Ocurrio algun error")
  }
}
export function* registerStudentSaga({payload:{resolve,reject,...payload}}){
  
  const {data} = yield select((state) => state);
  try{
    yield put(spinTrue());
    const url = "http://localhost:3000/api/students";
    const option = postOptionsWithoutToken(payload);
    const registerUser = yield call(request, url, option);
    console.log(registerUser)
    yield all([
      put(registerStudentStart([...data,registerUser])),
      put(spinFalse())
    ])
    yield call(resolve,"Se registro con exito")
  }catch(e){
    yield call(reject,"No se pudo registrar en la base de datos")
    yield put(spinFalse())
  }
}
export function* deleterStundetSaga({payload}){
  const {data} = yield select((state) => state);
  try{
  yield put(spinTrue());
  const url=`http://localhost:3000/api/students/${payload?.id}`
  const option=deleteOptionsWithoutToken();
  const deleterRegister=yield call(request,url,option)
  yield all([
    put(deleteStudentStart(data.filter(e=>e.id!==payload?.id))),
    put(spinFalse())
  ])
  message.success("Se elimino con exito")
  }catch(e){
    put(spinFalse())
    message.error("Ocurrio un error al eliminar el usuario")
  }
}
export function* updateStudentSaga({payload:{resolve,reject,...payload}}){
  const {data} = yield select((state) => state);
  try{
    yield put(spinTrue());
    const url = `http://localhost:3000/api/students/${payload?.id}`;
    const option = patchOptionsWithoutToken(payload);
    const updateStudent = yield call(request, url, option);
    yield all([
      put(updateStudentStart(data.map(e=>e.id===payload?.id?payload:e))),
      put(spinFalse())
    ])
    yield call(resolve,"Se Actualizo con exito")
  }catch(e){
    yield call(reject,"No se pudo Actualizar")
    yield put(spinFalse())
  }
}

export function* sagaLogin() {
  yield takeLatest(LOGIN_USER_SUCCESS, loginUser);
  yield takeLatest(INITIAL_REQUEST_STUDENTS_SUCCESS,initialRequestSaga)
  yield takeLatest(REGISTER_STUDENT_SUCCESS,registerStudentSaga)
  yield takeLatest(DELETE_STUDENT_SUCCESS,deleterStundetSaga)
  yield takeLatest(UPDATE_STUDENT_SUCCESS,updateStudentSaga)
}
