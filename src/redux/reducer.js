import { DELETE_STUDENT_START, INITIAL_REQUEST_STUDENTS_START, LOGIN_USER_START, REGISTER_STUDENT_START, SPIN_FALSE, SPIN_TRUE, UPDATE_STUDENT_START } from "./constants";

const initialState = {
  auth: {},
  data: [],
  spin: false,
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER_START:
      return { ...state, auth: action.payload };
    case SPIN_TRUE:
      return { ...state, spin: true };
    case SPIN_FALSE:
      return { ...state, spin: false };
    case INITIAL_REQUEST_STUDENTS_START:
      return{...state,data:action.payload}
    case REGISTER_STUDENT_START:
      return{...state,data:action.payload}
    case DELETE_STUDENT_START:
      return{...state,data:action.payload}
    case UPDATE_STUDENT_START:
      return{...state,data:action.payload}
    default:
      return state;
  }
};

export default profileReducer;
