//revisar el status de la peticion
function checkStatus(response) {
	if (response.status >= 200 && response.status < 300) {
		return response;
	}

	const error = new Error(response.status);
	error.response = response;
	throw error;
}
//convertir de json
function parseJSON(response) {
	return response.status === 204 ? "" : response.json();
}

//peticion 
export default function request(url, options) {
  return fetch(url, options).then(checkStatus).then(parseJSON);
}

//peticion post sin token
export function postOptionsWithoutToken(body = {}, method = "POST") {
	return {
		method,
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
		body: JSON.stringify(body),
	};
}
//peticion get sin token
export function getOptionsWithoutToken(method = "GET") {
	return {
		method,
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
	};
}
//peticion eliminar sin token
export function deleteOptionsWithoutToken(body, method = "DELETE") {
	return {
		method,
		headers: {
			Accept: "application/json",
		},
		body: JSON.stringify(body),
	};
}

//peticion actualizar datos
export function patchOptionsWithoutToken(body = {}, method = "PATCH") {
	return {
		method,
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
		body: JSON.stringify(body),
	};
}